clear all; close all;
% Load in data
load fishData % whitney fish data
load hedgeBestAngles.mat % my hedging angles from eyeballing + T&E


savePicsFlag = false; % this flag controls whether or not pictures are saved

% Make some counting variables
trialSCount = 0; % number of skips
trialLCount = 0; % number of disqualifications for loopiness
trialPCount = 0; % number of probalmatic trials
hedge = [];

for trial = 1:70
    skipFlag = false; % flag controls whether or not trial is skipped
    if hedgeBestAngles{trial} =='s';
        fprintf('trial %d Skipped\n',trial);
        trialFlag = 's'; % s for skip
        trialSCount = trialSCount+1;
        skipFlag = true;
    elseif hedgeBestAngles{trial} == 'l'
        trialFlag = 'l'; % l for loopy
        trialLCount = trialLCount+1;
        phi0 = 0*pi/180; % show pure pursuit for fun
    elseif hedgeBestAngles{trial} == 'p'
        trialFlag = 'p'; % p for problamatic (hedging doesn't work
        trialPCount = trialPCount+1;
        phi0 = 0*pi/180; % show pure pursuit for fun
    else
        phi0 = hedgeBestAngles{trial}*pi/180; % if it wasn't those other things, it was hedge angle
        hedge = [hedge phi0]; % keep track of all the hedge angles used
        trialFlag = 'g';
    end
    
    
    if ~skipFlag
        % Extract data from whitney data (less typing and more clear later)
        vt = d(trial).sp.spdPy; % prey speed
        vp = d(trial).sp.spdPd; % predator speed
        time = d(trial).sp.t; % time at each spline point
        timeODE = time;
        
        rp = d(trial).sp.xPd.coefs(1:end-2)+1j*d(trial).sp.yPd.coefs(1:end-2); % predator path in complex coord
        rt = d(trial).sp.xPy.coefs(1:end-2)+1j*d(trial).sp.yPy.coefs(1:end-2); % prey path in complex coord
        thetaPy = d(trial).sp.thetaPy; % prey heading
        thetaPd = d(trial).sp.thetaPd; % predator heading
        
        % Choose control parameters
        K = 15;
%         phi0 = 0*pi/180;
        
        % Initial Conditions
        y0 = [rp(1); rt(1); thetaPd(1)]; % choose IC same as real data
        % some exceptions for probalamatic cases (explained in spreadsheet, usually pred didn't see prey for the beginning part)
        if trial ==25
            idx = 120;
            timeODE = time(idx:end); % predator didn't notice prey until then (maybe)
            y0 = [rp(idx); rt(idx); thetaPd(idx)]; % choose IC same as real data
            phi0 = 4*pi/180;
        end
        if trial ==47
            idx = 60;
            timeODE = time(idx:end); % predator didn't notice prey until then (maybe)
            y0 = [rp(idx); rt(idx); thetaPd(idx)]; % choose IC same as real data
            phi0 = 12*pi/180;
        end
        if trial == 51
            phi0 = 12*pi/180;
        end
        if trial ==58
            idx = 700;
            timeODE = time(idx:end); % predator didn't notice prey until then (maybe)
            y0 = [rp(idx); rt(idx); thetaPd(idx)]; % choose IC same as real data
            phi0 = -2*pi/180;
        end
        if trial ==64
            idx = 280;
            timeODE = time(idx:end); % predator didn't notice prey until then (maybe)
            y0 = [rp(idx); rt(idx); thetaPd(idx)]; % choose IC same as real data
            phi0 = 1*pi/180;
        end
        if trial ==67
            idx = 175;
            timeODE = time(idx:end); % predator didn't notice prey until then (maybe)
            y0 = [rp(idx); rt(idx); thetaPd(idx)]; % choose IC same as real data
            phi0 = 1*pi/180;
        end 
        if trial ==50 % this was categorized as loopy
            idx = 75;
            timeODE = time(idx:end); % predator didn't notice prey until then (maybe)
            y0 = [rp(idx); rt(idx); thetaPd(idx)]; % choose IC same as real data
            phi0 = 12*pi/180;
        end
        
        % Simulate system
        [~ , yout] = ode45(@(t,y) dynHedgeODE_realFish(t,y,vp,vt,K,phi0,time,thetaPy),timeODE,y0);
        rp_dynHedge = yout(:,1); % prey position by hedging control law.
        rt_dynHedge = yout(:,2); % pred position from ODE45 (should be same as actual)
        
        % Plotting
        figure; hold on;
        plot(rt,'LineWidth',3);
        plot(rp,'LineWidth',3);
        plot(rp_dynHedge,'LineWidth',3);
        legend('Prey','Pred','dynHedge','Location','Best');
        xlabel('x (m)');
        ylabel('y (m)');
        hTitle = title(['Trial ' num2str(trial), ' | Flag = ',trialFlag, ' | K = ' num2str(K),' | \phi_0 = ' sprintf('%i%c', round(phi0*180/pi), char(176))]);
        if trialFlag~='g'; hTitle.Color = 'r'; end
        
        %%  Distance of curves
%         rpDistTrav = sum(abs(diff(rp)));
%         rpDistTrav_dynHedge = sum(abs(diff(rp_dynHedge)));
        
        
        if savePicsFlag
            axis image;
            saveas(gcf,['C:\Users\bfree\Google Drive\GradSchool\2017Summer\UCI_Visit\Whitney\hedgeTrials\trial' num2str(trial) '.png'])
            close;
        end
        %         close
    end
end
