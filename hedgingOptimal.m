function phiOptimal = hedgingOptimal

% headgingOptimal takes the code that Brian Free developed to simulate
% trajectories using the whitney data and different directional control
% laws. Alberto Soto adapted the code to run an optimization routine to
% find the best hedging angle 'phiOptimal'.
%
% OUTPUT:
%   phiOptimal: [70x2] array; 
%               optimal hedging angle; error index
%


clear all; close all;

% Load in data
load fishData % Whitney trajectory data

load hedgeBestAngles.mat % Brian's hedging angles from eyeballing + T&E

% This flag controls whether or not pictures are saved
savePicsFlag = false; 

% Make some counting variables
trialSCount = 0; % number of skips
trialLCount = 0; % number of disqualifications for loopiness
trialPCount = 0; % number of probalmatic trials
hedge = [];

% Create output variable
phiOptimal = NaN(70,2);

for trial = 1:70
    
    % Flag controls whether or not trial is skipped
    skipFlag = false;
    
    if hedgeBestAngles{trial} =='s';
        fprintf('trial %d Skipped\n',trial);
        trialFlag = 's'; % s for skip
        trialSCount = trialSCount+1;
        skipFlag = true;
    elseif hedgeBestAngles{trial} == 'l'
        trialFlag = 'l'; % l for loopy
        trialLCount = trialLCount+1;
        phi0 = 0*pi/180; % show pure pursuit for fun
    elseif hedgeBestAngles{trial} == 'p'
        trialFlag = 'p'; % p for problamatic (hedging doesn't work
        trialPCount = trialPCount+1;
        phi0 = 0*pi/180; % show pure pursuit for fun
    else
        phi0 = hedgeBestAngles{trial}*pi/180; % if it wasn't those other things, it was hedge angle
        hedge = [hedge phi0]; % keep track of all the hedge angles used
        trialFlag = 'g';
    end
    
    
    if ~skipFlag
        
        % Extract data from whitney data (less typing and more clear later)
        % Prey speed
        vt = d(trial).sp.spdPy; 
        
        % Predator speed
        vp = d(trial).sp.spdPd; 
        
        % Time at each spline point
        time = d(trial).sp.t; 
        
        % Time vector for solving ODEs
        timeODE = time;
        
        % Predator path in complex coord
        rp = d(trial).sp.xPd.coefs(1:end-2)+1j*d(trial).sp.yPd.coefs(1:end-2); 
        
        % Prey path in complex coord
        rt = d(trial).sp.xPy.coefs(1:end-2)+1j*d(trial).sp.yPy.coefs(1:end-2);
        
        % Prey heading
        thetaPy = d(trial).sp.thetaPy; 
        
        % Predator heading
        thetaPd = d(trial).sp.thetaPd; 
        
        % Choose control parameters
        K = 15;
%         phi0 = 0*pi/180;
        
        % Initial Conditions
        y0 = [rp(1); rt(1); thetaPd(1)]; % choose IC same as real data
        
        % Index to begin looking at data (default is 1)
        idx = 1;
        
        % some exceptions for probalamatic cases (explained in spreadsheet,
        % usually pred didn't see prey for the beginning part)
        
        if trial ==25
            idx = 120;
            timeODE = time(idx:end); % predator didn't notice prey until then (maybe)
            y0 = [rp(idx); rt(idx); thetaPd(idx)]; % choose IC same as real data
            phi0 = 4*pi/180;
        end
        
        if trial ==47
            idx = 60;
            timeODE = time(idx:end); % predator didn't notice prey until then (maybe)
            y0 = [rp(idx); rt(idx); thetaPd(idx)]; % choose IC same as real data
            phi0 = 12*pi/180;
        end
        
        if trial == 51
            phi0 = 12*pi/180;
        end
        
        if trial == 58
            idx = 700;
            timeODE = time(idx:end); % predator didn't notice prey until then (maybe)
            y0 = [rp(idx); rt(idx); thetaPd(idx)]; % choose IC same as real data
            phi0 = -2*pi/180;
        end
        if trial == 64
            idx = 280;
            timeODE = time(idx:end); % predator didn't notice prey until then (maybe)
            y0 = [rp(idx); rt(idx); thetaPd(idx)]; % choose IC same as real data
            phi0 = 1*pi/180;
        end
        
        if trial == 67
            idx = 175;
            timeODE = time(idx:end); % predator didn't notice prey until then (maybe)
            y0 = [rp(idx); rt(idx); thetaPd(idx)]; % choose IC same as real data
            phi0 = 1*pi/180;
        end 
        
        if trial == 50 % this was categorized as loopy
            idx = 75;
            timeODE = time(idx:end); % predator didn't notice prey until then (maybe)
            y0 = [rp(idx); rt(idx); thetaPd(idx)]; % choose IC same as real data
            phi0 = 12*pi/180;
        end
        
        % Store variables into 'D' structure
        D.vp      = vp;
        D.vt      = vt;
        D.K       = K;
        D.time    = time;
        D.thetaPy = thetaPy;
        D.timeODE = timeODE;
        D.y0      = y0;
        D.rp      = rp;
        D.idx     = idx;
        
        % Set initial guess to zero
        phi0 = 0;
        
        % Run the optimization to find optimal hedging angle
        % syntax: [phiOpt,fval] = runHedgeOptim(phi0,D,errType,method)
        [phiOpt,errMin] = runHedgeOptim(phi0,D,4,'search');
        
        disp(['Trial ' num2str(trial)])
        disp(['    The optimal hedging angle is...' num2str(phiOpt*180/pi)])
        
        % Store current optimal hedging angle
        phiOptimal(trial,1) = phiOpt(1);
        
        % Store current minimum error
        phiOptimal(trial,2) = errMin;
        
%         % Call ODE solver and run with optimal hedging
%         [~, yout] = ode45(@(t,y) dynHedgeODE_realFish(t,y,vp,vt,K,phiOpt,time,thetaPy),timeODE,y0);
%         
%         % pred position by hedging control law.
%         rp_dynHedge = yout(:,1);
        
        % prey position from ODE45 (should be same as actual)
%         rt_dynHedge = yout(:,2);

        % Plotting
%         figure; hold on;
%         plot(rt,'LineWidth',3);
%         plot(rp,'LineWidth',3);
%         plot(rp_dynHedge,'LineWidth',3);
%         legend('Prey','Pred','dynHedge','Location','Best');
%         xlabel('x (m)');
%         ylabel('y (m)');
%         hTitle = title(['Trial ' num2str(trial), ' | Flag = ',trialFlag, ' | K = ' num2str(K),' | \phi_0 = ' sprintf('%i%c', round(phi0*180/pi), char(176))]);
%         if trialFlag~='g'; hTitle.Color = 'r'; end
        
        if savePicsFlag
            axis image;
            saveas(gcf,['C:\Users\bfree\Google Drive\GradSchool\2017Summer\UCI_Visit\Whitney\hedgeTrials\trial' num2str(trial) '.png'])
            close;
        end
        %         close
    end
end






