function dy = dynHedgeODE_realFish(t,y,vp,vt,K,phi0,time,thetaPy)
%
% dynHedgeODE_realFish defines the dynamic equations for the Whitney lab
% experiments. 
%


% predator position
rp = y(1); 

% prey position
rt = y(2); 

% predator heading in inertial frame
thetaPd = y(3); 

% use pred speed data from experiment (sorta cheating)
vpNow = interp1(time,vp,t); 

% use prey speed data from experiment
vtNow = interp1(time,vt,t); 

% use prey heading data from experiment
thetaPyNow = interp1(time,thetaPy,t);  

% derivative of pred position (output to solver)
rpd = vpNow*exp(1j*thetaPd); 

% derivative of prey position (output to solver)
rtd = vtNow*exp(1j*thetaPyNow);

% Relative position vector between predator and prey (line of sight)
r = rt-rp;

% line of sight angle
alpha =  angle(r); 

% pred heading rate (by control law)
thetaPd_d = K*((alpha-phi0)-thetaPd); 

dy =[ rpd; rtd; thetaPd_d]; % collect outputs for ODE45
end
