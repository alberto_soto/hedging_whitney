function [phiOpt,fval] = runHedgeOptim(phi0,D,errType,method)
%
% INPUTS:   - phi0: the initial guess, needed for fminsearch()
%           - D:    structure of variables needed to run ODE solver
%           - errType:  type of error function 
%                       1 = Root Mean Squared
%                       2 = Mean Absolute Error 
%                       3 = Mean Euclidean Distance
%                       4 = Sum of Squared Residuals (SSR)
%           - method: optimization method 
%                     'search' = use fminsearch()
%                     'bounded'= use fminbnd()
%
% OUTPUTS:  - phiOpt: optimal hedging angle
%           - fval: value of the error index
%

% Unpack 'D' structure
rp      = D.rp;
vp      = D.vp;
vt      = D.vt;
K       = D.K;  % comment when optimization K 
time    = D.time;
thetaPy = D.thetaPy;
timeODE = D.timeODE;
y0      = D.y0;
idx     = D.idx;
        
% Run the optimization

switch method
    
    case 'search'
% Use 'fminsearch' by calling fminsearch(@fun,init,options)
[phiOpt,fval] = fminsearch(@objectiveFun,phi0);

    case 'bounded'
% Use 'fminbnd' by calling fminbnd(@fun,a,b)
[phiOpt,fval] = fminbnd(@objectiveFun,-20*pi/180,45*pi/180);

end
    
    % Nested function that computes error index
    function err_indx = objectiveFun(init)
        
        phi0 = init(1);
        
        % Call ODE solver
        [~, yout] = ode45(@(t,y) ...
            dynHedgeODE_realFish(t,y,vp,vt,K,phi0,time,thetaPy),timeODE,y0);
        
        % pred position by hedging control law.
        rp_dynHedge = yout(:,1);
        
        % Compute error index between simulated and actual trajectory
        err_indx = calcDist(rp_dynHedge,rp(idx:end),errType);

    end
end

% Function to compute error index from trajectories 
function err_indx = calcDist(rp_dynHedge,rp,errType)

% x-coordinates
xModel  = real(rp_dynHedge);
xTrue   = real(rp)';

% y-coordinates
yModel  = imag(rp_dynHedge);
yTrue   = imag(rp)';

% Squared residuals in x-coordinate
xRS = (xTrue-xModel).^2;

% Squared residuals in y-coordinate
yRS = (yTrue-yModel).^2;

% % Standard deviations of experiment data
% xSTD = std(xTrue);
% ySTD = std(yTrue);
% 
% % Number of elements
% N = length(xTrue);

% Standard error of data
% xSE = xSTD / sqrt(N);
% ySE = ySTD / sqrt(N);

% Choose type of error index based on inpute 'errType'
if errType==1
    
    % Root Mean Squared Error
    err_indx = sqrt(mean(xRS + yRS));
    
elseif errType==2
    
    % Mean Absolute Error (mean of L_1 norm)
    err_indx = mean( abs(xTrue-xModel) + abs(yTrue-yModel));
    
elseif errType==3
    
    % Mean Euclidean Distance (mean of L_2 norm)
    err_indx = mean(sqrt(xRS + yRS));
    
else
    
    % Sum of Squared Residuals (SSR)
    err_indx = sum(xRS + yRS);
end

end

%%%-----Extra equations, may be useful later------%%%%%
% % Sum of coordinates
% xSum = real(rp_dynHedge(1:end-1)) + real(rp_dynHedge(2:end));
% ySum = imag(rp_dynHedge(1:end-1)) + imag(rp_dynHedge(2:end));
%
% % Difference of coordinates
% xDiff = diff(real(rt_dynHedge));
% yDiff = diff(imag(rt_dynHedge));
%
% % Area of region enclosed by real and simulated trajectory
% err_indx = 0.5*(sum(xSum .* yDiff) - sum(ySum .* xDiff));


%------------------------------------------------------------------------%
% The optimal hedging angle is the angle that minimizes the following
% function:
%
%           sum_i^n [(n)^2 * sqrt((x-x_i)^2 + (y-y_i)^2)]
%
% The function weighs the later values of the trajectory more heavily than
% the values early in the sequence. This criteria was selected based on the
% actual trajectories in which the bearing angle converges toward a
% particular value as the predator gets closer to the prey. 
